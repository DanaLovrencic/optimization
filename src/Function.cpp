#include "Function.hpp"

#include <math.h>

unsigned Function::get_n_of_calls() const
{
    return call_counter;
}

double Function::operator()(const Point& x)
{
    call_counter++;
    return f(x);
}

Point DerivableFunction::df(const Point& x)
{
    gradient_counter++;
    return df_(x);
}

unsigned DerivableFunction::get_n_of_gradient_calls() const
{
    return gradient_counter;
}

Matrix SecondDerivableFunction::df2(const Point& x)
{
    hesse_counter++;
    return df2_(x);
}

unsigned SecondDerivableFunction::get_n_of_hesse_calls() const
{
    return hesse_counter;
}

TransformableFunction::TransformableFunction(
    std::function<double(const Point&)> f)
    : f_lambda(f)
{}

double TransformableFunction::f(const Point& x) const
{
    return f_lambda(x);
}

ConstrainedFunction::ConstrainedFunction(Function& func,
                                         const InequalityConstraints& ics,
                                         const EquationConstraints& ecs,
                                         double t)
    : func(func), ics(ics), ecs(ecs), t(t)
{}

double ConstrainedFunction::f(const Point& x) const
{
    double sum_of_ics = 0;

    if (!ics.are_satisfied(x)) {
        return std::numeric_limits<double>::infinity();
    } else {
        for (const auto& constraint : ics) {
            sum_of_ics -= (1 / t) * std::log(constraint->g_(x));
        }
    }

    double sum_of_ecs = 0;
    for (const auto& constraint : ecs) {
        sum_of_ecs += t * std::pow(constraint->h_(x), 2);
    }

    return func(x) + sum_of_ics + sum_of_ecs;
}

void ConstrainedFunction::multiply_t_with(double factor)
{
    t *= factor;
}

double F1::f(const Point& x) const
{
    return 100 * std::pow(x[1] - std::pow(x[0], 2), 2) + std::pow(1 - x[0], 2);
}

Point F1::df_(const Point& x) const
{
    std::vector<double> result(x.size());

    result[0] = -400 * (x[1] - std::pow(x[0], 2)) * x[0] - 2 * (1 - x[0]);
    result[1] = 200 * (x[1] - std::pow(x[0], 2));

    return Point(result);
}

Matrix F1::df2_(const Point& x) const
{
    Matrix result(x.size(), x.size());

    result(0, 0) = -400 * (x[1] - 3 * std::pow(x[0], 2)) + 2;
    result(0, 1) = -400;
    result(1, 0) = -400 * x[0];
    result(1, 1) = 200;

    return result;
}

double F2::f(const Point& x) const
{
    return std::pow(x[0] - 4, 2) + 4 * std::pow(x[1] - 2, 2);
}

Point F2::df_(const Point& x) const
{
    Point result(x.size());

    result[0] = 2 * (x[0] - 4);
    result[1] = 8 * (x[1] - 2);

    return result;
}

Matrix F2::df2_(const Point& x) const
{
    Matrix result(x.size(), x.size());

    result(0, 0) = 2;
    result(0, 1) = 0;
    result(1, 0) = 0;
    result(1, 1) = 8;

    return result;
}

double F3::f(const Point& x) const
{
    return std::pow(x[0] - 2, 2) + std::pow(x[1] + 3, 2);
}

Point F3::df_(const Point& x) const
{
    Point result(x.size());

    result[0] = 2 * (x[0] - 2);
    result[1] = 2 * (x[1] + 3);

    return result;
}

double F4::f(const Point& x) const
{
    return std::pow(x[0] - 3, 2) + std::pow(x[1], 2);
}

Point F4::df_(const Point& x) const
{
    Point result(x.size());

    result[0] = 2 * (x[0] - 2);
    result[1] = 2 * (x[1] + 3);

    return result;
}
