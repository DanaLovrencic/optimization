#include "Optimization.hpp"
#include "Function.hpp"

#include <cmath>
#include <stdexcept>
#include <random>

Point find_inner_point(const Point& start, InequalityConstraints& ics)
{
    TransformableFunction tf([&](const Point& x) {
        double sum = 0;

        for (auto& constraint : ics) {
            if (!constraint->is_satisfied(x)) { sum += constraint->g_(x); }
        }
        return -sum;
    });
    HookeJeeves hooke;
    return hooke.optimize(tf, start);
}

GradientDescent::GradientDescent(double error, double gamma)
    : error(error), gamma(gamma)
{}

Point GradientDescent::optimize(DerivableFunction& f,
                                const Point& start,
                                bool optimal_step)
{
    Point current(start.size());
    Point next = start;
    Point step(start.size());

    // For divergence check.
    Point previous_step(start.size());
    unsigned count_same = 0;

    do {
        current           = next;
        Point derivations = f.df(current);
        previous_step     = step;
        if (!optimal_step) {
            step = -derivations;
        } else {
            TransformableFunction tf([&](const Point& lambda) {
                Point transformed_current(current.size());
                transformed_current = current + lambda[0] * derivations;
                return f(transformed_current);
            });

            GoldenSection gs;
            double lambda = gs.optimize(0.0, tf);
            step          = lambda * derivations;
        }
        next = current + step;

        previous_step.norm() == step.norm() ? count_same++ : count_same = 0;
    } while (step.norm() > error && count_same < 100);

    return next;
}

NewtonRaphson::NewtonRaphson(double error) : error(error) {}

Point NewtonRaphson::optimize(SecondDerivableFunction& f,
                              const Point& start,
                              bool optimal_step)
{
    Point x = start;
    Point step(start.size());

    do {
        Point derivations   = -f.df(x);
        Matrix derivations_ = create_matrix(derivations);
        Matrix J            = f.df2(x);
        Matrix result       = J.LUP_solve(derivations_, 1e-6);
        step                = create_point(result);

        if (!optimal_step) {
            x = x + step;
        } else {
            TransformableFunction tf([&](const Point& lambda) {
                Point transformed(x.size());
                transformed = x + lambda[0] * derivations;
                return f(transformed);
            });

            GoldenSection gs;
            double lambda = gs.optimize(0.0, tf);
            x             = x + lambda * step;
        }
    } while (step.norm() > error);
    return x;
}

// One iteration of algorithm.
Point Box::optimize(Function& f,
                    const Point& start,
                    ExplicitConstraint& explicit_constraint,
                    InequalityConstraints& inequality_constraints)
{
    // Check whether starting point is valid.
    if (!explicit_constraint.is_satisfied(start) ||
        !inequality_constraints.are_satisfied(start)) {
        throw std::runtime_error("~~~~~~Constraints not satisfied~~~~~~.");
    }

    unsigned h     = 0;
    Point centroid = start;

    std::vector<Point> simplex = generate_simplex(
        centroid, explicit_constraint, inequality_constraints, f);

    do {
        h           = find_index_of_worst(simplex, f);
        unsigned h2 = find_index_of_second_worst(simplex, f, h);

        centroid              = calculate_centroid(h, simplex);
        Point reflected_point = (1 + alpha) * centroid - alpha * simplex[h];

        if (!explicit_constraint.is_satisfied(reflected_point)) {
            explicit_constraint.set_on_bound(reflected_point);
        }

        while (!inequality_constraints.are_satisfied(reflected_point)) {
            reflected_point = 0.5 * (reflected_point + centroid);
        }

        if (f(reflected_point) > f(simplex[h2])) {
            reflected_point = 0.5 * (reflected_point + centroid);
        }

        // Switch worst point with the new one.
        simplex[h] = reflected_point;

    } while ((simplex[h] - centroid).norm() > error);

    return centroid;
}

Point HookeJeeves::optimize(Function& f, const Point& start)
{
    Point xb = start;
    Point xp = start;
    Point xn(xb.size());

    do {
        xn = explore(xp, f);
        if (f(xn) < f(xb)) {
            for (unsigned i = 0; i < start.size(); i++) {
                xp[i] = 2 * xn[i] - xb[i];
            }
            xb = xn;
        } else {
            delta_x /= 2;
            xp = xb;
        }
    } while (delta_x > epsilon);

    return xb;
}

Point HookeJeeves::explore(Point xp, Function& f)
{
    Point x = xp;
    for (unsigned i = 0; i < xp.size(); i++) {
        double p = f(x);
        x[i] += delta_x;
        double n = f(x);
        if (n > p) {
            x[i] -= 2 * delta_x;
            n = f(x);
            if (n > p) { x[i] += delta_x; }
        }
    }
    return x;
}

std::vector<Point>
Box::generate_simplex(Point& centroid,
                      ExplicitConstraint& explicit_constraint,
                      InequalityConstraints& inequality_constraints,
                      Function& f)
{
    std::vector<Point> simplex;
    simplex.push_back(centroid);

    for (unsigned t = 0; t < 2 * centroid.size() - 1; t++) {
        Point random = create_random_point(centroid.size());
        Point x      = explicit_constraint.get_lower() +
                  random * (explicit_constraint.get_upper() -
                            explicit_constraint.get_lower());
        while (!inequality_constraints.are_satisfied(x)) {
            x = 0.5 * (x + centroid);
        }
        simplex.push_back(x);

        unsigned h = find_index_of_worst(simplex, f);
        centroid   = calculate_centroid(h, simplex);
    }
    return simplex;
}

unsigned Box::find_index_of_worst(const std::vector<Point>& points,
                                  Function& f) const
{
    Function& f_            = f; // Don't count this function calls.
    unsigned index_of_worst = 0;
    double worst_f          = f_(points[index_of_worst]);

    for (unsigned i = 0; i < points.size(); i++) {
        if (f(points[i]) > worst_f) {
            index_of_worst = i;
            worst_f        = f_(points[i]);
        }
    }

    return index_of_worst;
}

unsigned Box::find_index_of_second_worst(const std::vector<Point>& points,
                                         Function& f,
                                         unsigned h) const
{
    Function& f_                   = f; // Don't count this function calls.
    unsigned index_of_worst        = h;
    unsigned index_of_second_worst = index_of_worst == 0 ? 1 : 0;
    double worst_f                 = f_(points[index_of_worst]);
    double second_worst_f          = f_(points[index_of_second_worst]);

    for (unsigned i = 1; i < points.size(); i++) {
        if (f_(points[i]) > second_worst_f && f_(points[i]) < worst_f) {
            index_of_second_worst = i;
            second_worst_f        = f_(points[i]);
        }
    }

    return index_of_second_worst;
}

Point Box::calculate_centroid(unsigned h, std::vector<Point> x)
{
    unsigned centroid_size = x[0].size();
    unsigned simplex_size  = x.size();
    Point centroid(centroid_size);

    for (unsigned i = 0; i < centroid_size; i++) { centroid[i] = 0; }

    for (size_t i = 0; i < simplex_size; i++) {
        if (i != h) { centroid = centroid + x[i]; }
    }

    return centroid / (simplex_size - 1);
}

GoldenSection::GoldenSection(double epsilon, double h) : epsilon(epsilon), h(h)
{}

double GoldenSection::optimize(double start, Function& f)
{
    UnimodalInterval u;
    Interval uni_interval = u.find(start, f, h);
    Interval interval     = optimize_with_interval(uni_interval, f);
    return (interval.a + interval.b) / 2;
}

Interval GoldenSection::optimize_with_interval(Interval interval, Function& f)
{
    constexpr double k = 1 / 1.61803398875;

    double a = interval.a;
    double b = interval.b;

    double c = b - k * (b - a);
    double d = a + k * (b - a);

    double fc = f(c);
    double fd = f(d);

    int n_of_iters = 0;

    while (b - a > epsilon) {
        if (fc < fd) {
            b  = d;
            d  = c;
            c  = b - k * (b - a);
            fd = fc;
            fc = f(c);
        } else {
            a  = c;
            c  = d;
            d  = a + k * (b - a);
            fc = fd;
            fd = f(d);
        }
        n_of_iters++;
    }

    return {a, b};
}

Interval UnimodalInterval::find(double start, Function& f, double h)
{
    unsigned step = 1;

    Point x0(start);
    Point l(start - h);
    Point r(start + h);
    Point m(start);

    double fm = f(x0);
    double fl = f(l);
    double fr = f(r);

    if (fm < fr && fm < fl) {
        return {l[0], r[0]};
    } else if (fm > fr) {
        do {
            l    = m;
            m    = r;
            fm   = fr;
            r[0] = start + h * (step *= 2);
            fr   = f(r);
        } while (fm > fr);
    } else {
        do {
            r    = m;
            m    = l;
            fm   = fl;
            l[0] = start - h * (step *= 2);
            fl   = f(l);
        } while (fm > fl);
    }
    return {l[0], r[0]};
}

Matrix create_matrix(const Point& point)
{
    Matrix result(point.size(), 1);

    for (unsigned i = 0; i < point.size(); i++) { result(i, 0) = point[i]; }

    return result;
}

Point create_point(const Matrix& matrix)
{
    Point result(matrix.get_n_of_rows());

    for (unsigned i = 0; i < matrix.get_n_of_rows(); i++) {
        result[i] = matrix(i, 0);
    }

    return result;
}

Point create_random_point(unsigned size)
{
    static std::random_device rd;
    static std::mt19937 e2(rd());
    static std::uniform_real_distribution<double> dist(0.0, 1.0);

    Point random_point(size);
    for (unsigned i = 0; i < size; i++) { random_point[i] = dist(e2); }

    return random_point;
}
