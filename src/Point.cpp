#include "Point.hpp"

#include <math.h>

Point::Point(std::vector<double> x) : x(x) {}

Point::Point(double x) : x({x}) {}

Point::Point(const std::initializer_list<double>& x) : x(x) {}

Point::Point(unsigned size)
{
    x.resize(size);
}

Point Point::normalize() const
{
    double point_norm = norm();
    std::vector<double> normalized;
    for (auto& el : x) normalized.push_back(el / point_norm);
    return Point(normalized);
}

double Point::norm() const
{
    double res = 0;
    for (auto& el : x) res += std::pow(el, 2);

    return std::sqrt(res);
}

unsigned Point::size() const
{
    return x.size();
}

Point Point::absolute() const
{
    Point p(size());
    for (unsigned i = 0; i < size(); i++) p[i] = std::abs(x[i]);
    return p;
}

double& Point::operator[](int index)
{
    return x.at(index);
}

double Point::operator[](int index) const
{
    return x.at(index);
}

std::ostream& operator<<(std::ostream& os, const Point& p)
{
    for (unsigned i = 0; i < p.size(); i++) { os << p[i] << " "; }
    os << "\n";
    return os;
}

bool operator==(const Point& first, const Point& second)
{
    constexpr double epsilon = 1e-3;
    for (unsigned i = 0; i < first.size(); i++) {
        if (std::abs(first[i] - second[i]) > epsilon) { return false; }
    }
    return true;
}

Point operator*(const Point& first, const Point& second)
{
    Point res(first.size());
    for (unsigned i = 0; i < first.size(); i++) res[i] = first[i] * second[i];
    return res;
}

Point operator*(double a, const Point& p)
{
    Point res(p.size());
    for (unsigned i = 0; i < p.size(); i++) res[i] = a * p[i];
    return res;
}

Point operator/(const Point& p, double a)
{
    Point res(p.size());
    for (unsigned i = 0; i < p.size(); i++) res[i] = p[i] / a;
    return res;
}

Point operator+(const Point& first, const Point& second)
{
    Point res(first.size());
    for (unsigned i = 0; i < first.size(); i++) res[i] = first[i] + second[i];
    return res;
}

Point operator-(const Point& first, const Point& second)
{
    Point res = first + (-second);
    return res;
}

Point operator-(const Point& p)
{
    Point res(p.size());
    for (std::size_t i = 0; i < p.size(); i++) res[i] = -p[i];
    return res;
}

bool operator<(const Point& l, const Point& r)
{
    for (unsigned i = 0; i < l.size(); i++) {
        if (!(l[i] < r[i])) return false;
    }
    return true;
}

bool operator>(const Point& l, const Point& r)
{
    return !(l < r);
}

std::ostream& operator<<(std::ostream& os, const std::vector<Point>& p)
{
    for (unsigned i = 0; i < p.size(); i++) { os << p[i]; }
    os << "\n";
    return os;
}
