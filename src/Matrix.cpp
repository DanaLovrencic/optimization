#include "Matrix.hpp"

#include <fstream>
#include <iostream>
#include <exception>


Matrix create_identity_matrix(unsigned n);

Matrix Matrix::LU_solve(Matrix b)
{
    Matrix p = create_identity_matrix(n_of_rows);
    LU_decomposition();
    Matrix y = forward_substitution(b, p);
    Matrix x = backward_substitution(y);

    return x;
}


Matrix Matrix::LUP_solve(Matrix b, double epsilon)
{
    Matrix p = create_identity_matrix(n_of_rows);
    p = LUP_decomposition(epsilon);
    Matrix y = forward_substitution(b, p);
    Matrix x = backward_substitution(y); 

    return x;
}


Matrix Matrix::inverse()
{
    Matrix inv(n_of_rows, n_of_cols);
    Matrix p = LUP_decomposition(1e-6);
    Matrix e = create_identity_matrix(n_of_rows);

    for (unsigned i = 0; i < n_of_cols; i++) {
        Matrix e_ = e.get_column(i);

        Matrix y = forward_substitution(e_, p);
        Matrix x = backward_substitution(y);

        inv.set_column(x, i);
    }

    return inv;
}

#include <math.h>

double Matrix::find_determinant() {
    Matrix p = LUP_decomposition(1e-6);
    Matrix lower = get_lower_matrix();
    Matrix upper = get_upper_matrix();

    double lower_product = 1;
    double upper_product = 1;
        
    for (unsigned i = 0; i < n_of_rows; i++) {
        lower_product *= lower(i, i);
        upper_product *= upper(i, i);
    }

    return std::pow(-1, n_of_switched_rows) * lower_product * upper_product;

}


void Matrix::operator*=(const Matrix& other)
{
    *this =  *this * other;
}

Matrix operator*(const Matrix& lhs, const Matrix& rhs)
{
    unsigned n_of_rows = lhs.n_of_rows;
    unsigned n_of_cols = rhs.n_of_cols;
    unsigned common_dim = lhs.n_of_cols;
    Matrix result(n_of_rows, n_of_cols);;

    /* Sum product of each row element with correspondent column element and
       set it to resulting matrix coefficient. */
    for (unsigned i = 0; i < n_of_rows; i++) {
        for (unsigned j = 0; j < n_of_cols; j++) {
            for (unsigned k = 0; k < common_dim; k++) {
                double temp_coeff = result(i, j) + lhs(i, k) * rhs(k, j);
                result(i, j) = temp_coeff;
            }
        }
    }

    return result;
}


void Matrix::operator*=(double scalar)
{
    for (unsigned i = 0; i < n_of_rows; i++) {
        for (unsigned j = 0; j < n_of_cols; j++) {
            (*this)(i, j) *= scalar; 
        }
    }
}


Matrix Matrix::operator*(double scalar)
{
    Matrix result(n_of_rows, n_of_cols);

    for (unsigned i = 0; i < n_of_rows; i++) {
        for (unsigned j = 0; j < n_of_cols; j++) {
            result(i, j) = (*this)(i, j) * scalar; 
        }
    }
    return result;
}


double Matrix::operator()(unsigned row_index, unsigned col_index) const
{
    return elements[row_index * n_of_cols + col_index];
}


double& Matrix::operator()(unsigned row_index, unsigned col_index)
{
    return elements[row_index * n_of_cols + col_index];
}


Matrix Matrix::operator~()
{
    Matrix transposed(n_of_cols, n_of_rows);

    for (unsigned i = 0; i < n_of_rows; i++) {
        for (unsigned j = 0; j < n_of_cols; j++) {
            transposed(j, i) = (*this)(i, j); 
        } 
    }

    return transposed;
}


std::ostream& operator<<(std::ostream &os, const Matrix& matrix)
{
    for (unsigned i = 0; i < matrix.n_of_rows; i++) {
        for (unsigned j = 0; j < matrix.n_of_cols; j++) {
            os << matrix(i, j) << " ";
        }
        os << '\n';
    }
    return os;
}


bool operator==(const Matrix& first, const Matrix& second) {
    return first.elements == second.elements;
}


void Matrix::write_matrix(const std::filesystem::path& file_name)
{
    std::ofstream fos(file_name);

    for (unsigned i = 0; i < n_of_rows; i++) {
        for (unsigned j = 0; j < n_of_cols; j++) {
            fos << (*this)(i, j) << " "; 
        } 
        fos << '\n';
    }
}

void Matrix::load_matrix(const std::filesystem::path& filepath)
{
    std::ifstream fis(filepath);

    std::string line;
    std::getline(fis, line);
    std::istringstream ss(line);

    double element;
    while (ss >> element) { elements.push_back(element); }
    n_of_cols = elements.size();

    while (fis >> element) { elements.push_back(element); }
    n_of_rows = elements.size() / n_of_cols;
}


void Matrix::fill_with_zeros()
{
    for (unsigned i = 0; i < n_of_rows * n_of_cols; i++) {
        elements.push_back(0.0); 
    }
}

Matrix create_identity_matrix(unsigned n)
{
    Matrix identity;
    identity.n_of_rows = n;
    identity.n_of_cols = n;

    for (unsigned i = 0; i < n; i++) {
        for (unsigned j = 0; j < n; j++) {
            if (i == j) identity.elements.push_back(1.0);
            else identity.elements.push_back(0.0); 
        }
    }

    return identity;
}


Matrix Matrix::get_lower_matrix()
{
    Matrix lower(n_of_rows, n_of_cols);

    for (unsigned i = 0; i < n_of_rows; i++) {
        for (unsigned j = 0; j < n_of_cols; j++) {
            if (i == j) lower(i, j) = 1;
            else if (i > j) lower(i, j) = (*this)(i, j);
            // Otherwise, zero is already written.
        } 
    }

    return lower;
}


Matrix Matrix::get_upper_matrix()
{
    Matrix upper(n_of_rows, n_of_cols);

    for (unsigned i = 0; i < n_of_rows; i++) {
        for (unsigned j = 0; j < n_of_cols; j++) {
            if (i <= j) upper(i, j) = (*this)(i, j); 
            // Otherwise, zero is already written.
        }
    }

    return upper;
}


Matrix Matrix::forward_substitution(const Matrix& b, const Matrix& p)
{
    Matrix y = p * b;
    Matrix lower = get_lower_matrix();

    for (unsigned i = 0; i < n_of_rows - 1; i++) {
        for (unsigned j = i + 1; j < n_of_cols; j++) {
            y(j, 0) -= lower(j, i) * y(i, 0);
        }
    }

    return y;
}


Matrix Matrix::backward_substitution(const Matrix& y)
{
    Matrix x = y;
    Matrix upper = get_upper_matrix();

    for (int i = (int)n_of_rows - 1; i >= 0; i--) {
        if (upper(i, i) == 0) { 
            throw std::runtime_error("~~~~~~Singular matrix~~~~~~.");
        }
        x(i, 0) /= upper(i, i);
        for (int j = 0; j < i; j++) {
            x(j, 0) -= upper(j, i) * x(i, 0); 
        }     
    }
    return x;
}


void Matrix::LU_decomposition() {
    for (unsigned i = 0; i < n_of_rows - 1; i++) {
        for (unsigned j = i + 1; j < n_of_cols; j++) {
            (*this)(j, i) /= (*this)(i, i); 
            for (unsigned k = i + 1; k < n_of_rows; k++) {
                (*this)(j, k) -=  (*this)(j, i) * (*this)(i, k); 
            }
        } 
    }
}


Matrix Matrix::LUP_decomposition(double epsilon) {
    Matrix p = create_identity_matrix(n_of_rows);

    unsigned pivot;
    for (unsigned i = 0; i < n_of_cols - 1; i++) {
        pivot = i;
        // Set pivot to the biggest element in the column. 
        for (unsigned j = i + 1; j < n_of_rows; j++) {
            // Element is bigger than current pivot.
            if (std::abs((*this)(pivot, i)) < std::abs((*this)(j, i))) {
                pivot = j;
            }
        }

        if (std::abs((*this)(pivot, i)) < epsilon) {
            throw std::runtime_error("~~~~~~Pivot equals zero~~~~~~.");
        }

        // Switch i-th and row where is the pivot.
        switch_rows(pivot, i);
        // Switch rows in identity matrix.
        if(p.switch_rows(pivot, i)) n_of_switched_rows++;;

        for (unsigned j = i + 1; j < n_of_rows; j++) {
            (*this)(j, i) /= (*this)(i, i); 
            if (std::abs((*this)(j, i)) < epsilon) (*this)(j, i) = 0;
            for (unsigned k = i + 1; k < n_of_cols; k++) {
                (*this)(j, k) -= (*this)(j, i) * (*this)(i, k); 
                // If element is close to zero, set it to zero.
                if (std::abs((*this)(j, k)) < epsilon) (*this)(j, k) = 0;
            }
        }
    }

    return p;
}


bool Matrix::switch_rows(unsigned index1, unsigned index2)
{
    if (index1 == index2) return false;
    std::vector<double> temp;

    // Store all elemenets with index1.
    for (unsigned j = 0; j < n_of_cols; j++) {
        temp.push_back((*this)(index1, j));
    }

    // Copy elements from row with index2 to row with index1.
    for (unsigned j = 0; j < n_of_cols; j++) {
        (*this)(index1, j) = (*this)(index2, j); 
    }
    
    // Copy elements stored in temp to row with second index.
    for (unsigned j = 0; j < n_of_cols; j++) {
        (*this)(index2, j) = temp[j];
    }
    return true;
}


Matrix Matrix::get_column(unsigned index)
{
    Matrix column(n_of_rows, 1);
    for (unsigned j = 0; j < n_of_rows; j++) {
        column(j, 0) = (*this)(j, index);
    }
    return column;
}


void Matrix::set_column(Matrix column, unsigned index)
{
    for (unsigned j = 0; j < n_of_rows; j++) {
        (*this)(j, index) = column(j, 0); 
    }
}

unsigned Matrix::get_n_of_rows() const
{
    return n_of_rows;
}
