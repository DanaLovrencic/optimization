#include <iostream>

#include "Function.hpp"
#include "Optimization.hpp"

int main(void)
{
    std::cout << "TASK1" << std::endl;
    {
        F3 f;
        Point start({0, 0});
        GradientDescent gd(1e-6, 1e-2);
        Point res = gd.optimize(f, start);
        std::cout << "GS without optimal step: " << res;
        std::cout << "f: " << f.get_n_of_calls() << "\n";
        std::cout << "df: " << f.get_n_of_gradient_calls() << "\n\n";
    }

    {
        F3 f;
        Point start({0, 0});
        GradientDescent gd(1e-6, 1e-2);
        Point res = gd.optimize(f, start, true);
        std::cout << "GS with optimal step: " << res;
        std::cout << "f: " << f.get_n_of_calls() << "\n";
        std::cout << "df: " << f.get_n_of_gradient_calls() << "\n\n";
    }

    std::cout << "TASK2" << std::endl;
    {
        F1 f;
        Point start({-1.9, 2});
        GradientDescent gd(1e-6, 1e-2);
        Point res = gd.optimize(f, start, true);
        std::cout << "GS f1: " << res;
        std::cout << "f: " << f.get_n_of_calls() << "\n";
        std::cout << "df: " << f.get_n_of_gradient_calls() << "\n\n";
    }

    {
        F2 f;
        Point start({0.1, 0.3});
        GradientDescent gd(1e-6, 1e-2);
        Point res = gd.optimize(f, start, true);
        std::cout << "GS f2: " << res;
        std::cout << "f: " << f.get_n_of_calls() << "\n";
        std::cout << "df: " << f.get_n_of_gradient_calls() << "\n\n";
    }

    {
        F1 f;
        Point start({-1.9, 2});
        NewtonRaphson nr;
        Point res = nr.optimize(f, start);
        std::cout << "NR f1: " << res;
        std::cout << "f: " << f.get_n_of_calls() << "\n";
        std::cout << "df: " << f.get_n_of_gradient_calls() << "\n";
        std::cout << "df2: " << f.get_n_of_hesse_calls() << "\n\n";
    }

    {
        F2 f;
        Point start({0.1, 0.3});
        NewtonRaphson nr;
        Point res = nr.optimize(f, start);
        std::cout << "NR f2: " << res;
        std::cout << "f: " << f.get_n_of_calls() << "\n";
        std::cout << "df: " << f.get_n_of_gradient_calls() << "\n";
        std::cout << "df2: " << f.get_n_of_hesse_calls() << "\n\n";
    }

    std::cout << "TASK3" << std::endl;
    {
        F1 f;
        Point start({-1.9, 2});
        Box box;
        InequalityConstraints ic;
        ic.add(std::make_unique<G1>());
        ic.add(std::make_unique<G2>());
        ExplicitConstraint ec(-100, 100, 2);
        Point res = box.optimize(f, start, ec, ic);
        std::cout << "BOX f1: " << res;
        std::cout << "f: " << f.get_n_of_calls() << "\n\n";
    }

    {
        F2 f;
        Point start({0.1, 0.3});
        Box box;
        InequalityConstraints ic;
        ic.add(std::make_unique<G1>());
        ic.add(std::make_unique<G2>());
        ExplicitConstraint ec(-100, 100, 2);
        Point res = box.optimize(f, start, ec, ic);
        std::cout << "BOX f2: " << res;
        std::cout << "f: " << f.get_n_of_calls() << "\n\n";
    }

    std::cout << "TASK4" << std::endl;
    {
        constexpr double epsilon = 1e-6;
        F1 f;
        Point start({-1.9, 2});

        InequalityConstraints ics;
        ics.add(std::make_unique<G1>());
        ics.add(std::make_unique<G2>());

        EquationConstraints ecs;

        ConstrainedFunction tf(f, ics, ecs);

        HookeJeeves hooke;
        Point current_res = hooke.optimize(tf, start);
        Point previous_res(start.size());
        Point epsilons({epsilon, epsilon});

        do {
            tf.multiply_t_with(10.0);
            previous_res = current_res;
            current_res  = hooke.optimize(tf, current_res);
        } while ((current_res - previous_res).absolute() > epsilons);

        std::cout << "TR f1: " << current_res;
        std::cout << "f: " << f.get_n_of_calls() << "\n\n";
    }

    {
        constexpr double epsilon = 1e-6;
        F2 f;
        Point start({0.1, 0.3});

        InequalityConstraints ics;
        ics.add(std::make_unique<G1>());
        ics.add(std::make_unique<G2>());

        EquationConstraints ecs;

        ConstrainedFunction tf(f, ics, ecs);

        HookeJeeves hooke;
        Point current_res = hooke.optimize(tf, start);
        Point previous_res(start.size());
        Point epsilons({epsilon, epsilon});

        do {
            tf.multiply_t_with(10.0);
            previous_res = current_res;
            current_res  = hooke.optimize(tf, current_res);
        } while ((current_res - previous_res).absolute() > epsilons);

        std::cout << "TR f2: " << current_res;
        std::cout << "f: " << f.get_n_of_calls() << "\n\n";
    }

    std::cout << "TASK5" << std::endl;
    {
        constexpr double epsilon = 1e-6;
        F4 f;
        Point start({5, 5});

        InequalityConstraints ics;
        ics.add(std::make_unique<G3>());
        ics.add(std::make_unique<G4>());

        EquationConstraints ecs;
        ecs.add(std::make_unique<H1>());

        ConstrainedFunction tf(f, ics, ecs);

        HookeJeeves hooke;
        Point current_res = hooke.optimize(tf, start);
        Point previous_res(start.size());
        Point epsilons({epsilon, epsilon});

        do {
            tf.multiply_t_with(10.0);
            previous_res = current_res;
            current_res  = hooke.optimize(tf, current_res);
        } while ((current_res - previous_res).absolute() > epsilons);

        std::cout << "TR f4: " << current_res;
        std::cout << "f: " << f.get_n_of_calls() << "\n\n";
    }

    {
        constexpr double epsilon = 1e-6;
        F4 f;

        InequalityConstraints ics;
        ics.add(std::make_unique<G3>());
        ics.add(std::make_unique<G4>());

        EquationConstraints ecs;
        ecs.add(std::make_unique<H1>());

        Point start = find_inner_point({5, 5}, ics);

        ConstrainedFunction tf(f, ics, ecs);

        HookeJeeves hooke;
        Point current_res = hooke.optimize(tf, start);
        Point previous_res(start.size());
        Point epsilons({epsilon, epsilon});

        do {
            tf.multiply_t_with(10.0);
            previous_res = current_res;
            current_res  = hooke.optimize(tf, current_res);
        } while ((current_res - previous_res).absolute() > epsilons);

        std::cout << "TR f4: " << current_res;
        std::cout << "f: " << f.get_n_of_calls() << "\n\n";
    }

    return 0;
}
