#include "Constraint.hpp"

Bound::Bound(double lower, double upper) : lower(lower), upper(upper) {}

ExplicitConstraint::ExplicitConstraint(std::vector<Bound> bounds)
    : bounds(bounds)
{}

ExplicitConstraint::ExplicitConstraint(double lower_bounds,
                                       double upper_bounds,
                                       unsigned n)
{
    for (unsigned i = 0; i < n; i++) {
        bounds.push_back(Bound(lower_bounds, upper_bounds));
    }
}

bool ExplicitConstraint::is_satisfied(const Point& x) const
{
    for (unsigned i = 0; i < x.size(); i++) {
        if (x[i] < bounds[i].lower || x[i] > bounds[i].upper) { return false; }
    }
    return true;
}

void ExplicitConstraint::set_on_bound(Point& x)
{
    for (unsigned i = 0; i < x.size(); i++) {
        if (x[i] < bounds[i].lower) {
            x[i] = bounds[i].lower;
        } else if (x[i] > bounds[i].upper) {
            x[i] = bounds[i].upper;
        }
    }
}

Point ExplicitConstraint::get_lower() const
{
    std::vector<double> lower_bounds;

    for (unsigned i = 0; i < bounds.size(); i++) {
        lower_bounds.push_back(bounds[i].lower);
    }

    return Point(lower_bounds);
}

Point ExplicitConstraint::get_upper() const
{
    std::vector<double> upper_bounds;

    for (unsigned i = 0; i < bounds.size(); i++) {
        upper_bounds.push_back(bounds[i].upper);
    }

    return Point(upper_bounds);
}

double EquationConstraint::operator()(const Point& x)
{
    return h(x);
}

double EquationConstraint::h_(const Point& x) const
{
    return h(x);
}

bool EquationConstraint::is_satisfied(const Point& x) const
{
    return is_satisfied_(x);
}

void EquationConstraints::add(std::unique_ptr<EquationConstraint> constraint)
{
    constraints.push_back(std::move(constraint));
}

bool EquationConstraints::are_satisfied(const Point& x) const
{
    for (auto& constraint : constraints) {
        if (!constraint->is_satisfied(x)) { return false; }
    }

    return true;
}

std::vector<std::unique_ptr<EquationConstraint>>::const_iterator
EquationConstraints::begin() const
{
    return constraints.begin();
}

std::vector<std::unique_ptr<EquationConstraint>>::const_iterator
EquationConstraints::end() const
{
    return constraints.end();
}

double InequalityConstraint::operator()(const Point& x)
{
    return g(x);
}

double InequalityConstraint::g_(const Point& x) const
{
    return g(x);
}

bool InequalityConstraint::is_satisfied(const Point& x) const
{
    return is_satisfied_(x);
}

void InequalityConstraints::add(
    std::unique_ptr<InequalityConstraint> constraint)
{
    constraints.push_back(std::move(constraint));
}

bool InequalityConstraints::are_satisfied(const Point& x) const
{
    for (auto& constraint : constraints) {
        if (!constraint->is_satisfied(x)) { return false; }
    }

    return true;
}

std::vector<std::unique_ptr<InequalityConstraint>>::const_iterator
InequalityConstraints::begin() const
{
    return constraints.begin();
}

std::vector<std::unique_ptr<InequalityConstraint>>::const_iterator
InequalityConstraints::end() const
{
    return constraints.end();
}

bool G1::is_satisfied_(const Point& x) const
{
    return x[1] - x[0] >= 0;
}

double G1::g(const Point& x) const
{
    return x[1] - x[0];
}

bool G2::is_satisfied_(const Point& x) const
{
    return 2 - x[0] >= 0;
}

double G2::g(const Point& x) const
{
    return 2 - x[0];
}

bool G3::is_satisfied_(const Point& x) const
{
    return 3 - x[0] - x[1] >= 0;
}

double G3::g(const Point& x) const
{
    return 3 - x[0] - x[1];
}

bool G4::is_satisfied_(const Point& x) const
{
    return 3 + 1.5 * x[0] - x[1] >= 0;
}

double G4::g(const Point& x) const
{
    return 3 + 1.5 * x[0] - x[1];
}

double H1::h(const Point& x) const
{
    return x[1] - 1;
}

bool H1::is_satisfied_(const Point& x) const
{
    return x[1] - 1 == 0;
}
