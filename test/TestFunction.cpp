#include "TestFunction.hpp"

#include <cmath>

double FTest::f(const Point& x) const
{
    return std::pow(x[0] - 3, 2);
}
