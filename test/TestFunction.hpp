#ifndef TEST_FUNCTION_HPP
#define TEST_FUNCTION_HPP

#include "Function.hpp"

class FTest : public Function {
  private:
    double f(const Point& x) const override;
};

#endif
