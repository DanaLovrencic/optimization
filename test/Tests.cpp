#include <catch2/catch.hpp>

#include "Optimization.hpp"
#include "Function.hpp"

#include "TestFunction.hpp"

#include <iostream>
#include <limits>

TEST_CASE("Function F1")
{
    F1 f;
    Point p({2, 3});
    REQUIRE(f(p) == 101);
    // REQUIRE(f.df(p) == Point({798, -200}));
}

TEST_CASE("Function F2")
{
    F2 f;
    Point p({2, 3});
    REQUIRE(f(p) == 8);
    REQUIRE(f.df(p) == Point({-4, 8}));
}

TEST_CASE("Point operators")
{
    Point p1({4, 5, 6});
    Point p2({1, 2, 4});
    double a = 2;

    REQUIRE(p1 - p2 == Point({3, 3, 2}));
    REQUIRE(-p1 == Point({-4, -5, -6}));
    REQUIRE(p1 + p2 == Point({5, 7, 10}));
    REQUIRE(a * p1 == Point({8, 10, 12}));
}

TEST_CASE("GD optimal F3")
{
    F3 f;
    Point start({0, 0});
    GradientDescent gd(1e-9, 1e-2);
    Point res = gd.optimize(f, start, true);

    REQUIRE(res == Point({2, -3}));
}

TEST_CASE("GD F3")
{
    F3 f;
    Point start({0, 0});
    GradientDescent gd(1e-6, 1e-2);
    Point res = gd.optimize(f, start);

    REQUIRE(res == Point({4, -6}));
}

TEST_CASE("GD F1")
{
    F1 f;
    Point start({-1.9, 2});
    GradientDescent gd(1e-6, 1e-2);
    Point res = gd.optimize(f, start);

    REQUIRE(res == Point({1, 1}));
}

TEST_CASE("Unimodal interval")
{
    FTest f;
    Point start(10.0);
    UnimodalInterval u;
    Interval i = u.find(start[0], f);
    REQUIRE(Interval(-6.0, 6.0) == i);
}

TEST_CASE("Golden section")
{
    FTest f;
    Point start(10.0);
    GoldenSection gs;
    double lambda = gs.optimize(start[0], f);
    REQUIRE(lambda == Approx(3.0));
}

TEST_CASE("Jacobian")
{
    F2 f;
    Point x({4, 2});
    Matrix res = f.df2(x);
}

// FAILS, TODO: fix optimal step size!!!!!!
TEST_CASE("NR F1")
{
    F1 f;
    Point start({-1.9, 2});
    NewtonRaphson nr(1e-6);
    Point res = nr.optimize(f, start);
    REQUIRE(res == Point({1, 1}));
}

TEST_CASE("NR F2")
{
    F2 f;
    Point start({0.1, 0.3});
    NewtonRaphson nr(1e-6);
    Point res = nr.optimize(f, start);
    REQUIRE(res == Point({4, 2}));
}

TEST_CASE("matrix from point")
{
    Point p({1, 2});
    Matrix m = create_matrix(p);
    // std::cout << m << std::endl;
}

TEST_CASE("point from matrix")
{
    Matrix m({1, 2}, 2, 1);
    Point p = create_point(m);
    REQUIRE(p == Point({1, 2}));
}

TEST_CASE("LUP J")
{
    F2 f;
    Point p({0, 0});
    Point derivations   = -f.df(p);
    Matrix derivations_ = create_matrix(derivations);
    Matrix m            = f.df2(p);
    Matrix res          = m.LUP_solve(derivations_, 1e-6);

    REQUIRE(create_point(res) == Point({4, 2}));
}

TEST_CASE("Box F2")
{
    InequalityConstraints ic;
    ic.add(std::make_unique<G1>());
    ic.add(std::make_unique<G2>());
    Box box;
    Point start({0.1, 0.3});
    F2 f;
    ExplicitConstraint ec(-100, 100, 2);
    Point res = box.optimize(f, start, ec, ic);
    std::cout << res << std::endl;
}

TEST_CASE("Box F1")
{
    InequalityConstraints ic;
    ic.add(std::make_unique<G1>());
    ic.add(std::make_unique<G2>());
    Box box;
    Point start({-1.9, 2});
    F1 f;
    ExplicitConstraint ec(-100, 100, 2);
    Point res = box.optimize(f, start, ec, ic);
    std::cout << res << std::endl;
}

TEST_CASE("ble")
{
    constexpr double epsilon = 1e-6;
    F2 f;
    Point start({0.1, 0.3});

    InequalityConstraints ics;
    ics.add(std::make_unique<G1>());
    ics.add(std::make_unique<G2>());

    EquationConstraints ecs;
    // ec.add(std::make_unique<H1>());

    ConstrainedFunction tf(f, ics, ecs);

    HookeJeeves hooke;
    Point current_res = hooke.optimize(tf, start);
    Point previous_res(start.size());
    Point epsilons({epsilon, epsilon});
    std::cout << "\nSTART RES: " << current_res << std::endl;
    do {
        tf.multiply_t_with(10.0);
        previous_res = current_res;
        current_res  = hooke.optimize(tf, current_res);
    } while ((current_res - previous_res).absolute() > epsilons);
    std::cout << tf.get_n_of_calls() << std::endl;

    std::cout << current_res << std::endl;
}
