#ifndef OPTIMIZATION_HPP
#define OPTIMIZATION_HPP

#include "Function.hpp"
#include "Interval.hpp"
#include "Constraint.hpp"

Point create_random_point(unsigned size);
Point find_inner_point(const Point& start,
                       InequalityConstraints& ics);

class GradientDescent {
  private:
    double error = 1e-6;
    double gamma = 1e-2;

  public:
    GradientDescent(double error, double gamma);
    GradientDescent() = default;
    Point optimize(DerivableFunction& f,
                   const Point& start,
                   bool optimal_step = false);
};

class NewtonRaphson {
  private:
    double error = 10e-6;

  public:
    NewtonRaphson(double error);
    NewtonRaphson() = default;
    Point optimize(SecondDerivableFunction& f,
                   const Point& start,
                   bool optimal_step = false);
};

class Box {
  private:
    double error = 10e-6;
    double alpha = 1.3;

  public:
    Point optimize(Function& f,
                   const Point& start,
                   ExplicitConstraint& explicit_constraint,
                   InequalityConstraints& inequality_constraints);

  private:
    std::vector<Point>
    generate_simplex(Point& centroid,
                     ExplicitConstraint& explicit_constraint,
                     InequalityConstraints& inequality_constraints,
                     Function& f);
    Point calculate_centroid(unsigned h, std::vector<Point> x);
    unsigned find_index_of_worst(const std::vector<Point>& points,
                                 Function& f) const;
    unsigned find_index_of_second_worst(const std::vector<Point>& points,
                                        Function& f,
                                        unsigned h) const;
};

class HookeJeeves {
  private:
    double epsilon = 1e-5;
    double delta_x = 1;

  public:
    Point optimize(Function& f, const Point& start);

  private:
    Point explore(Point xp, Function& f);
};

class GoldenSection {
  private:
    double epsilon = 1e-6;
    double h       = 1;

  public:
    GoldenSection(double epsilon, double h);
    GoldenSection() = default;
    double optimize(double start, Function& f);

  private:
    Interval optimize_with_interval(Interval interval, Function& f);
};

class UnimodalInterval {
  public:
    Interval find(double start, Function& f, double h = 1);
};

// Helper methods.
Matrix create_matrix(const Point& point);
Point create_point(const Matrix& matrix);

#endif
