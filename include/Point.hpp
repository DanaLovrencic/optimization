#ifndef POINT_HPP
#define POINT_HPP

#include <vector>
#include <ostream>

class Point {
  private:
    std::vector<double> x;

  public:
    Point(std::vector<double> x);
    Point(const std::initializer_list<double>& x);
    Point(double x);
    explicit Point(unsigned size);
    Point normalize() const;
    double norm() const;
    unsigned size() const;
    Point absolute() const;
    double operator[](int index) const;
    double& operator[](int index);

    friend std::ostream& operator<<(std::ostream& os, const Point& p);
    friend bool operator==(const Point& first, const Point& second);
    friend Point operator*(double a, const Point& p);
    friend Point operator/(const Point& p, double a);
    friend Point operator*(const Point& first, const Point& second);
    friend Point operator+(const Point& first, const Point& second);
    friend Point operator-(const Point& first, const Point& second);
    friend Point operator-(const Point& p);
    friend bool operator<(const Point& l, const Point& r);
    friend bool operator>(const Point& l, const Point& r);
};

std::ostream& operator<<(std::ostream& os, const std::vector<Point>& p);

#endif
