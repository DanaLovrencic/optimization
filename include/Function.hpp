#ifndef FUNCTION_HPP
#define FUNCTION_HPP

#include "Point.hpp"
#include "Matrix.hpp"
#include "Constraint.hpp"

#include "functional"

class Function {
  private:
    unsigned call_counter = 0;

  public:
    unsigned get_n_of_calls() const;
    double operator()(const Point& x);

  private:
    virtual double f(const Point& x) const = 0;
};

class DerivableFunction : public Function {
  private:
    unsigned gradient_counter = 0;

  public:
    Point df(const Point& x);
    unsigned get_n_of_gradient_calls() const;

  private:
    virtual Point df_(const Point& x) const = 0;
};

class SecondDerivableFunction : public DerivableFunction {
  private:
    unsigned hesse_counter = 0;

  public:
    Matrix df2(const Point& x);
    unsigned get_n_of_hesse_calls() const;

  private:
    virtual Matrix df2_(const Point& x) const = 0;
};

class TransformableFunction : public Function {
  private:
    std::function<double(const Point&)> f_lambda;

  public:
    TransformableFunction(std::function<double(const Point&)> f);
    double f(const Point& x) const override;
};

class ConstrainedFunction : public Function {
  private:
    Function& func;
    const InequalityConstraints& ics;
    const EquationConstraints& ecs;
    double t = 1.0;

  public:
    ConstrainedFunction(Function& func,
                        const InequalityConstraints& ics,
                        const EquationConstraints& ecs,
                        double t = 1.0);

    double f(const Point& x) const override;
    void multiply_t_with(double factor);
};

/* Functions. */
class F1 : public SecondDerivableFunction {
  private:
    double f(const Point& x) const override;
    Point df_(const Point& x) const override;
    Matrix df2_(const Point& x) const override;
};

class F2 : public SecondDerivableFunction {
  private:
    double f(const Point& x) const override;
    Point df_(const Point& x) const override;
    Matrix df2_(const Point& x) const override;
};

class F3 : public DerivableFunction {
  private:
    double f(const Point& x) const override;
    Point df_(const Point& x) const override;
};

class F4 : public DerivableFunction {
  private:
    double f(const Point& x) const override;
    Point df_(const Point& x) const override;
};

#endif
