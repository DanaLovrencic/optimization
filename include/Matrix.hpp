#ifndef MATRIX_HPP
#define MATRIX_HPP

#include <vector>
#include <ostream>
#include <filesystem>

/** Representation of 2D matrix with arbitrary dimensions. */
class Matrix {
private:
    std::vector<double> elements;
    unsigned n_of_rows, n_of_cols;
    unsigned n_of_switched_rows = 0;


public:
    /** Creates matrix filled with data from given file. */
    Matrix(const std::filesystem::path& filepath) { load_matrix(filepath); }

    /** Creates matrix filled with data from given vector. */
    Matrix(std::vector<double> elements, unsigned n_of_rows, unsigned n_of_cols)
        : elements(elements), n_of_rows(n_of_rows), n_of_cols(n_of_cols) {}

    /** Creates empty (uninitialized) matrix. */
    Matrix() = default;

    /** Creates matrix filled with zeros. */
    Matrix(unsigned n_of_rows, unsigned n_of_cols)
        : n_of_rows(n_of_rows), n_of_cols(n_of_cols) { fill_with_zeros(); }


    /** Solves system of equations using LU decomposition. */
    Matrix LU_solve(Matrix b);

    /** Solves system of equations using LUP decomposition. */
    Matrix LUP_solve(Matrix b, double epsilon);

    /** Finds inverse of a matrix using LUP decomposition. */
    Matrix inverse();

    /** Finds determinant of a matrix using LUP decomposition. */
    double find_determinant();


    /** Multiplies this (lhs) by other (rhs) matrix. */
    void operator*=(const Matrix& other);

    /** Multiplies matrix by scalar. */
    void operator*=(double scalar);

    /** Adds this (lhs) and other (rhs) matrix. */
    Matrix operator+=(const Matrix& other);

    /** Multiplies matrix by scalar. */
    Matrix operator*(double scalar);

    /** Transposes matrix. */
    Matrix operator~();


    /** Sets/returns element on given position. */
    double operator()(unsigned row_index, unsigned col_index) const;

    /** Sets/returns element on given position. */
    double& operator()(unsigned row_index, unsigned col_index);


    /** Writes matrix elements in file. */
    void write_matrix(const std::filesystem::path& filepath);

    /** Returns number of rows. */
    unsigned get_n_of_rows() const;


    /** Prints matrix to the standard output */
    friend std::ostream& operator<<(std::ostream &os, const Matrix& matrix);

    /** Multiplies left-hand side by right-hand side matrix. */
    friend Matrix operator*(const Matrix& lhs, const Matrix& rhs);

    /** Adds first and second matrix. */
    friend Matrix operator+(const Matrix& first, const Matrix& second);

    /** Compares given matrices. */
    friend bool operator==(const Matrix& first, const Matrix& second);

private:
    /** Finds y in matrix equation L * y = P * b. */
    Matrix forward_substitution(const Matrix& rhs_vector, const Matrix& p);

    /** Finds x in matrix equation U * x = y. */
    Matrix backward_substitution(const Matrix& rhs_vector);

    /** Decomposites system matrix on L and U matrices. */
    void LU_decomposition();

    /** Decomposites system matrix on L and U matrices using partial pivoting. */
    Matrix LUP_decomposition(double epsilon);

    /** Loads matrix elements from given file (and sets proper dimensions). */
    void load_matrix(const std::filesystem::path& filepath);

    /** Fills uninitialized matrix with zeros (and sets dimensions). */
    void fill_with_zeros();

    /** Creates lower matrix from system matrix.*/
    Matrix get_lower_matrix();

    /** Creates upper matrix from system matrix.*/
    Matrix get_upper_matrix();

    /** Creates identity matrix. */
    friend Matrix create_identity_matrix(unsigned n);

    /** Switches rows with given indices. */
    bool switch_rows(unsigned i, unsigned j);

    /** Returns single column from a matrix. */
    Matrix get_column(unsigned index);

    /** Sets column to the given values. */
    void set_column(Matrix column, unsigned index);

};

#endif
